using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using MediatR;
using Persistencia;

namespace Aplicacion.Comentarios
{
    public class Eliminar
    {
        public class Ejecuta : IRequest{
            public Guid ComentarioId {get;set;}
        }

        public class Manejador : IRequestHandler<Ejecuta>
        {
            private readonly CursosOnlineContext _context;
            public Manejador(CursosOnlineContext context){
                 _context = context;
            }
            public async Task<Unit> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                //var comentario = _context.Comentario.FindAsync(request.ComentarioId);
                var comentario = _context.Comentario.Where(x => x.ComentarioId == request.ComentarioId).FirstOrDefault();
                if(comentario == null){
                    throw new HandlerException(HttpStatusCode.NotFound, new{mensaje = "No se puede eliminar el comentario"});
                }
                _context.Remove(comentario);
                var resultado = await _context.SaveChangesAsync();
                if(resultado > 0){
                    return Unit.Value;
                }
                throw new Exception("No se ha podido eliminar el comentario");
            }
        }
    }
}
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Persistencia.DapperConexion.Instructor;
using Aplicacion.HandlerError;

namespace Aplicacion.Instructores
{
    public class ConsultaId
    {
        // aquí pongo InstructorModelo porque es el dato que va a devolver IRequest
        public class Ejecuta : IRequest<InstructorModelo>{
            public Guid Id {get;set;}
        }

        public class Manejador : IRequestHandler<Ejecuta, InstructorModelo>
        {
            private readonly IInstructor _instructorRepository;
            public Manejador(IInstructor instructorRepository){
                _instructorRepository = instructorRepository;
            }

            public async Task<InstructorModelo> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                var instructor = await _instructorRepository.ObtenerPorId(request.Id);
                if(instructor == null){
                    throw new HandlerException(HttpStatusCode.NotFound, new{mensaje = "No se ha encontrado el instructor"});
                }
                return instructor;
            }
        }
    }
}
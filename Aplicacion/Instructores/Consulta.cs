using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Persistencia.DapperConexion.Instructor;

namespace Aplicacion.Instructores
{
    public class Consulta
    {
         /* Tanto si queremos invocar a la lógica de Entity Framework como la de Dapper tenemos
        que crear dos clases */
        public class Lista : IRequest<List<InstructorModelo>>{}
        // IRequest aquí devuelve un List<InstructorModelo>
        public class Manejador : IRequestHandler<Lista,List<InstructorModelo>>{

            private readonly IInstructor _instructorRepository;

            public Manejador(IInstructor instructorRepository){

                _instructorRepository = instructorRepository;

            }

            public async Task<List<InstructorModelo>> Handle(Lista request, CancellationToken cancellation){
                var resultado = await _instructorRepository.ObtenerLista();
                return resultado.ToList();
            }
        }
    }
}
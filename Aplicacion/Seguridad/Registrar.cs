using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.Contratos;
using Aplicacion.HandlerError;
using Dominio;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistencia;

namespace Aplicacion.Seguridad
{
    public class Registrar
    {
        public class Ejecuta : IRequest<UsuarioData>
        {
            public string Nombre { get; set; }
            public string Apellidos { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public string UserName { get; set; }
        }

        // En esta clase validamos que los datos no llegan vacíos o nulos, heredamos de AbstractValidator
        // que evaluará a la clase Ejecuta
        public class EjecutaValidador : AbstractValidator<Ejecuta>
        {
            public EjecutaValidador(){
            RuleFor(x => x.Nombre).NotEmpty();
            RuleFor(x => x.Apellidos).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
            RuleFor(x => x.UserName).NotEmpty();
            }

        }

        // IRequestHandler Ejecuta es el parámetro que recibe, UsuarioData es lo que devuelve
        public class Manejador : IRequestHandler<Ejecuta, UsuarioData>
        {
            private readonly CursosOnlineContext _context;
            private readonly UserManager<Usuario> _userManager;
            private readonly iJwtGenerador _jwtGenerador;
            public Manejador(CursosOnlineContext context, UserManager<Usuario> userManager,
            iJwtGenerador jwtGenerador)
            {
                _context = context;
                _userManager = userManager;
                _jwtGenerador = jwtGenerador;
            }


            public async Task<UsuarioData> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                // Primero comprobamos que el email no existe, Where pertenece a la librería LinQ y la 
                // x representa todos los registros de la tabla users
                var existe = await _context.Users.Where(x => x.Email == request.Email).AnyAsync();
                // AnyAsync devuelve un flag, un valor booleano
                if (existe)
                {
                    throw new HandlerException(HttpStatusCode.BadRequest, new { mensaje = "El email ya existe" });
                }
                var existeUserName = await _context.Users.Where(x => x.UserName == request.UserName).AnyAsync();
                if (existeUserName)
                {
                    throw new HandlerException(HttpStatusCode.BadRequest, new { mensaje = "Ya existe un usuario con ese nombre" });
                }
                // Si no existe el usuario lo creamos
                var usuario = new Usuario
                {
                    NombreCompleto = request.Nombre + " " + request.Apellidos,
                    Email = request.Email,
                    UserName = request.UserName
                };

                // Este método es el que nos permite insertar el nuevo usuario
                var resultado = await _userManager.CreateAsync(usuario, request.Password);
                Console.WriteLine(resultado);
                if (resultado.Succeeded)
                {
                    return new UsuarioData
                    {
                        NombreCompleto = usuario.NombreCompleto,
                        Token = _jwtGenerador.CrearToken(usuario),
                        UserName = usuario.UserName,
                        Email = usuario.Email
                    };
                }
                throw new Exception("No se pudo agregar el nuevo usuario");
            }
        }
    }
}
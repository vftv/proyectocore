using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.Contratos;
using Aplicacion.HandlerError;
using Dominio;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Aplicacion.Seguridad
{
    public class Login
    {
        // Esta es la cabecera donde llamará el controller de WebAPI y tendrá que pasar estos 2 parámetros
        public class Ejecuta : IRequest<UsuarioData>{
            public string Email {get;set;}
            public string Password {get;set;}
        }
        // Esta clase es para comprobar que los parámetros no entren vacios
        public class EjecutaValidacion : AbstractValidator<Ejecuta>{ //Abstract viene de FluentValidator
            public EjecutaValidacion(){
                RuleFor(x => x.Email).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();
            }
        }

        // Esta esla lógica, le pasamos dos parámetros, el primero es Ejecuta(que ya contiene los parámetros),
        // el segundo es el tipo de entidad que va a trabajar en este caso Usuario
        public class Manejador : IRequestHandler<Ejecuta, UsuarioData>
        {
            private readonly UserManager<Usuario> _userManager;
            private readonly SignInManager<Usuario> _signInManager;

            private readonly iJwtGenerador _jwtGenerador;

            // Para poder trabajar la lógica necesitamos inyectar 2 objetos (UserManager y SignInManager), estos 
            // objetos vienen de Core Identity y referencian a la BD para hacer la comparación del email y psw
            public Manejador(UserManager<Usuario> userManager, SignInManager<Usuario> signInManager, 
            iJwtGenerador jwtGenerador){
                _userManager = userManager;
                _signInManager = signInManager;
                _jwtGenerador = jwtGenerador;
            }
            // Implementamos la interfaz IRequestHandler
            public async Task<UsuarioData> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                var usuario = await  _userManager.FindByEmailAsync(request.Email);
                if(usuario == null){
                    throw new HandlerException(HttpStatusCode.Unauthorized);
                }
               // Si el usuario si existe hacemos el login usando el SignInManager inyectado
               var resultado = await _signInManager.CheckPasswordSignInAsync(usuario, request.Password,false);
               if(resultado.Succeeded){
                   return new UsuarioData{
                       NombreCompleto = usuario.NombreCompleto,
                       Token = _jwtGenerador.CrearToken(usuario),
                       UserName = usuario.UserName,
                       Email = usuario.Email,
                       Imagen = null
                   };
               }

                throw new HandlerException(HttpStatusCode.Unauthorized);
            }
        }
    }
}
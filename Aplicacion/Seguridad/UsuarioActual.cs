using System.Threading;
using System.Threading.Tasks;
using Aplicacion.Contratos;
using Aplicacion.Interfaces;
using Microsoft.AspNetCore.Identity;
using Dominio;
using MediatR;

namespace Aplicacion.Seguridad
{
    public class UsuarioActual
    {
        public class Ejecutar : IRequest<UsuarioData>{

        }

        public class Manejador : IRequestHandler<Ejecutar, UsuarioData>
        {
            private readonly UserManager<Usuario> _userManager;
            private readonly iJwtGenerador _JwtGenerador;
            private readonly IUsuariosSesion _UsuarioSesion;
            
            public Manejador(UserManager<Usuario> userManager, iJwtGenerador JwtGenerador, IUsuariosSesion UsuarioSesion){
                _userManager = userManager;
                _JwtGenerador = JwtGenerador;
                _UsuarioSesion = UsuarioSesion;
            }
            public async Task<UsuarioData> Handle(Ejecutar request, CancellationToken cancellationToken)
            {
                var usuario = await _userManager.FindByNameAsync(_UsuarioSesion.ObtenerUsuarioSesion());
                return new UsuarioData
                    {
                        NombreCompleto = usuario.NombreCompleto,
                        Token = _JwtGenerador.CrearToken(usuario),
                        UserName = usuario.UserName,
                        Email = usuario.Email,
                        Imagen = null
                    };
            }
        }
    }
}
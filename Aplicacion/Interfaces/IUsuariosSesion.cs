namespace Aplicacion.Interfaces
{
    public interface IUsuariosSesion
    {
        string ObtenerUsuarioSesion();
    }
}
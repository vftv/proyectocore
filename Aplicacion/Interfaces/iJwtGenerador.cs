using Dominio;

namespace Aplicacion.Contratos
{
    public interface iJwtGenerador
    {
        string CrearToken(Usuario usuario);
    }
}
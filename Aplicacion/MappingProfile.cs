using System.Linq;
using Aplicacion.Cursos;
using AutoMapper;
using Dominio;

namespace Aplicacion
{
    public class MappingProfile : Profile
    {
        public MappingProfile(){
            // La clase Curso se va a mapear contra la clase CursoDTO, pero la tabla no tiene la lista de 
            // instructores, entonces le tenemos que decir de donde saca el miembro listaInstructores
            CreateMap<Curso, CursoDTO>()
            .ForMember(x => x.Instructores, y => y.MapFrom(z=> z.InstructorLink.Select(a => a.Instructor).ToList()))
            .ForMember(x => x.Comentarios, y => y.MapFrom(z => z.ComentarioLista))
            .ForMember(x => x.Precio, y => y.MapFrom(z => z.PrecioPromocion));
            // x.Instructores proviene del DTO, 'y' y 'a' provienen de EntityCore
            // ForMember y MapFrom son de la librería AutoMapping
            // Select es de la librería Linq
            CreateMap<Instructor, InstructorDTO>();
            CreateMap<CursoInstructor, CursoInstructorDTO>();
            CreateMap<Comentario, ComentarioDTO>();
            CreateMap<Precio, PrecioDTO>();
        }
    }
}
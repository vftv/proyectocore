using System;
using System.Collections;
using System.Collections.Generic;
using Dominio;

namespace Aplicacion.Cursos
{
    public class CursoDTO
    {
        public Guid CursoId {get; set;}

        public string Titulo {get; set;}

        public string Descripcion {get; set;}

        public DateTime? FechaPublicacion {get; set;}
        
        public Byte[] fotoPortada {get;set;}

        // Cada curso debe tener su lista de instructores
        public ICollection<InstructorDTO> Instructores {get;set;}

        public PrecioDTO Precio {get;set;}

        public ICollection<ComentarioDTO> Comentarios {get;set;}
    }
}
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using MediatR;
using Persistencia;

namespace Aplicacion.Cursos
{
    public class Eliminar
    {
        public class Ejecuta : IRequest{
            public Guid CursoId {get; set;}
        }

        public class Manejador : IRequestHandler<Ejecuta>
        {
            public readonly CursosOnlineContext _Context;

            public Manejador(CursosOnlineContext context){
                _Context = context;
            }
            public async Task<Unit> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                var instructoresDB = _Context.CursoInstructor.Where(x => x.CursoId == request.CursoId);
                // Eliminamos la lista de instructores que tiene cada curso en la tabla CursoInstructor
                foreach(var instructor in instructoresDB){
                    _Context.CursoInstructor.Remove(instructor);
                }

                var comentarioDB = _Context.Comentario.Where(x => x.CursoId == request.CursoId);
                foreach(var cmt in comentarioDB){
                    _Context.Comentario.Remove(cmt);
                }

                var precioDB = _Context.Precio.Where(x => x.CursoId == request.CursoId).FirstOrDefault();
                if(precioDB != null){
                    _Context.Precio.Remove(precioDB);
                }

                var curso = await _Context.Curso.FindAsync(request.CursoId);
                if(curso == null){
                    //throw new Exception("El curso no existe");
                    throw new HandlerException(HttpStatusCode.NotFound, new {curso = "No se encontró el curso"});
                }

                _Context.Remove(curso);

                var resultado = await _Context.SaveChangesAsync();
                if(resultado>0){
                    return Unit.Value;
                }
                throw new Exception("No se ha podido eliminar el curso");
            }
        }
    }
}
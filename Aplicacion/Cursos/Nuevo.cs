using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Dominio;
using FluentValidation;
using MediatR;
using Persistencia;

namespace Aplicacion.Cursos
{
    public class Nuevo
    {
        public class Ejecuta : IRequest{
        public string Titulo {get; set;}

        public string Descripcion {get; set;}

        public DateTime? FechaPublicacion {get; set;}
        // Este parámetro es para la tabla CursoInstructor 
        public List<Guid> ListaInstructor {get;set;}
        public decimal Precio {get;set;}
        public decimal Promocion {get;set;}
        }

        // En esta clase validamos que los datos no llegan vacíos o nulos, heredamos de AbstractValidator
            // que evaluará a la clase Ejecuta
        public class Ejecutavalidacion : AbstractValidator<Ejecuta>
            {
                public Ejecutavalidacion()
                {
                    RuleFor(x => x.Titulo).NotEmpty();
                    RuleFor(x => x.Descripcion).NotEmpty();
                    RuleFor(x => x.FechaPublicacion).NotEmpty();
                }
            }

        public class Manejador : IRequestHandler<Ejecuta>
        {

            private readonly CursosOnlineContext _context;
            public Manejador(CursosOnlineContext context){
                _context = context;
            }
            // CancellationToken cancela la operacion (por si tarda mucho)
            public async Task<Unit> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                // creamos un id aleatorio
                Guid _cursoId =  Guid.NewGuid();

                var curso = new Curso {
                    CursoId = _cursoId,
                    Titulo = request.Titulo,
                    Descripcion = request.Descripcion,
                    FechaPublicacion = request.FechaPublicacion
                };

                // agregamos el nuevo curso al contexto
                _context.Curso.Add(curso);

                if(request.ListaInstructor != null){
                    foreach(var  id in request.ListaInstructor){
                        // Por cada id en ListaInstructor vamos a crear un nuevo record para guardar en ListaInstructor
                        var cursoInstructor = new CursoInstructor{
                            CursoId = _cursoId,
                            InstructorId = id
                        };
                    // agregamos los instructores al contexto    
                    _context.CursoInstructor.Add(cursoInstructor);

                    }
                }

                var precioEntidad = new Precio{
                    CursoId = _cursoId,
                    PrecioActual = request.Precio,
                    Promocion = request.Promocion,
                    PrecioId = Guid.NewGuid()
                };
                _context.Precio.Add(precioEntidad);
                
                // Hacemos el commit de la transacción en la BD,si devuelve 0 es que hubo un error
                // SaveChangesAsync devuelve el nº de operaciones realizadas en la BD
                var valor = await _context.SaveChangesAsync();

                if(valor > 0){
                    return Unit.Value; // Devolvemos un flagg
                }

                // Sino lanzamos una excepción en todo el programa.
                throw new Exception("No se pudo insertar el curso");
            }
        }
    }
}
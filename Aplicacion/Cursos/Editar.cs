using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using Dominio;
using MediatR;
using Persistencia;

namespace Aplicacion.Cursos
{
    public class Editar
    {
        public class Ejecuta : IRequest{
        public Guid CursoId {get; set;}

        public string Titulo {get; set;}

        public string Descripcion {get; set;}

        public DateTime? FechaPublicacion {get; set;} // ? es para que permita nulos ya que datetime no permite null
        public List<Guid> ListaInstructor {get;set;}
         public decimal? Precio {get;set;} // Con el interrogante indicamos que pueden ser nulos
        public decimal? Promocion {get;set;}
        }

        public class Manejador : IRequestHandler<Ejecuta>
        {
            private readonly CursosOnlineContext _context;

            public Manejador(CursosOnlineContext context){
                _context = context;
            }
            public async Task<Unit> Handle(Ejecuta request, CancellationToken cancellationToken)
            {
                // Buscamos el curso por el parámetro CursoId.
                var curso = await _context.Curso.FindAsync(request.CursoId);
                if(curso == null){
                    throw new HandlerException(HttpStatusCode.NotFound, new {curso = "No se encontró el curso"});
                }

                // Comprobamos si el valor que pasamos es null dejamos el que había
                curso.Titulo = request.Titulo ?? curso.Titulo;
                curso.Descripcion = request.Descripcion ?? curso.Descripcion;
                curso.FechaPublicacion = request.FechaPublicacion ?? curso.FechaPublicacion;
                //  actualizar el precio del curso
                // FirstOrDefault devuelve el primer valor que cumple esta condición, aunque solo hay un valor
                // hay que ponerlo de todas formas
                var precioEntidad = _context.Precio.Where(x => x.CursoId == curso.CursoId).FirstOrDefault();
                if(precioEntidad != null){
                    // Si request.Promocion es nulo entonces dejamos precioEntidad.Promocion
                    precioEntidad.Promocion = request.Promocion ?? precioEntidad.Promocion;
                    precioEntidad.PrecioActual = request.Precio ?? precioEntidad.PrecioActual;
                }else{
                    precioEntidad = new Precio{
                        PrecioId = Guid.NewGuid(),
                        PrecioActual = request.Precio ?? 0,
                        Promocion = request.Promocion ?? 0,
                        CursoId = curso.CursoId
                    };
                    await _context.Precio.AddAsync(precioEntidad);
                }

                if(request.ListaInstructor != null){
                    if(request.ListaInstructor.Count > 0){
                        // Eliminar los instructores actuales en la BD
                        var instructoresBD = _context.CursoInstructor.Where(x => x.CursoId == request.CursoId).ToList();
                        foreach(var instructorEliminar in instructoresBD){
                            _context.CursoInstructor.Remove(instructorEliminar);
                        }

                        // Ahora agregamos los instructores que provienen del cliente
                        foreach(var id in request.ListaInstructor){
                            var nuevoInstructor = new CursoInstructor{
                                CursoId = request.CursoId,
                                InstructorId = id
                            };
                            _context.CursoInstructor.Add(nuevoInstructor);
                        }
                    }
                }
                // Si resultado devuelve 0 es que hubo error
                var resultado = await _context.SaveChangesAsync();
                if(resultado>0){
                    return Unit.Value;
                }

                throw new Exception("No se han guardado los cambios en el curso");
            }
        }
    }
}
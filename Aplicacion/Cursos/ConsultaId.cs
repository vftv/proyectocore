using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using AutoMapper;
using Dominio;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistencia;

namespace Aplicacion.Cursos
{
    public class ConsultaId
    {
        // Esta clase será la cabecera de la consulta donde podemos añadir parámetros
        // Aquí es donde el controller del WebAPI llamará para ingresar parámetros
        public class CursoUnico : IRequest<CursoDTO>{
            public Guid Id {get;set;}
        }

        // Esta clase contendrá la lógica
        public class Manejador : IRequestHandler<CursoUnico, CursoDTO>
        {
            private readonly CursosOnlineContext _context;
            private readonly IMapper _mapper;

            // Necesita un constructor
            public Manejador(CursosOnlineContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            // Este método es obligatorio implementarlo
            public async Task<CursoDTO> Handle(CursoUnico request, CancellationToken cancellationToken)
            {
                // El request viene de la clase CursoUnico que contiene la propiedad Id
                var curso = await _context.Curso
                .Include(x => x.ComentarioLista)
                .Include(x => x.PrecioPromocion)
                .Include(x => x.InstructorLink)
                .ThenInclude(y => y.Instructor)
                .FirstOrDefaultAsync(a => a.CursoId == request.Id);
                if(curso == null){
                    throw new HandlerException(HttpStatusCode.NotFound, new {curso = "No se encontró el curso"});
                }

                var cursoDto = _mapper.Map<Curso, CursoDTO>(curso);
                return cursoDto;// Esto son los datos que devuelve (lo que veremos)
            }
        }
    }
}
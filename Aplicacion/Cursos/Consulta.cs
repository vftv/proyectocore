using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Dominio;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistencia;

namespace Aplicacion.Cursos
{
    public class Consulta
    {
        public class ListaCursos : IRequest<List<CursoDTO>>{

        }

        public class Manejador : IRequestHandler<ListaCursos, List<CursoDTO>>
        {
            private readonly CursosOnlineContext _context;
            private readonly IMapper _mapper;
            public Manejador(CursosOnlineContext context, IMapper mapper){
                _context = context;
                _mapper = mapper;
            }
            // IRequestHandler nos obliga a implementar este método:
            public async Task<List<CursoDTO>> Handle(ListaCursos request, CancellationToken cancellationToken)
            {
                // InstructorLink representa la tabla CursoInstructor
                var cursos = await _context.Curso
                .Include(x => x.ComentarioLista)
                .Include(x => x.PrecioPromocion)
                .Include(x => x.InstructorLink)
                .ThenInclude(x => x.Instructor).ToListAsync(); // Aquí incluyo la tabla instructor
                // .Map pide 2 parámetros el tipo de dato origen y eltipo de datos destino(en que se va a convertir)
                // y por últimolos datos que vas a convertir
                var cursosDto = _mapper.Map<List<Curso>, List<CursoDTO>>(cursos);
                return cursosDto; // Esto son los datos que devuelve (lo que veremos)
            }
        }
    }
}
using Microsoft.AspNetCore.Identity;

namespace Dominio
{
    public class Usuario : IdentityUser // IdentityUser ya tienes muchos atributos como Email, UserName, etc
    {
        public string NombreCompleto {get;set;}
    }
}
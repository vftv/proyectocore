using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Dominio;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Persistencia
{
    public class CursosOnlineContext : IdentityDbContext<Usuario>
    {
        public CursosOnlineContext(DbContextOptions options) : base(options){

        }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder){
            // Creamos el archivo de migración que va a tener toda la lógica que creará las tablas
            base.OnModelCreating(modelBuilder);
            //Aquí le decimos que CursoInstructor tiene 2 claves primarias
            modelBuilder.Entity<CursoInstructor>().HasKey(ci => new{ci.CursoId, ci.InstructorId});
        }

        public DbSet<Curso> Curso {get;set;}
        public DbSet<Precio> Precio {get;set;}
        public DbSet<Comentario> Comentario {get;set;}
        public DbSet<Instructor> Instructor {get;set;}
        public DbSet<CursoInstructor> CursoInstructor {get;set;}
    }
}
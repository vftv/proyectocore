using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using System.Linq;

namespace Persistencia.DapperConexion.Instructor
{
    public class InstructorRepositorio : IInstructor
    {
        public readonly IFactoryConnection _factoryConnection;

        // En el constructor inyectamos la conexion
        public InstructorRepositorio(IFactoryConnection factoryConnection){
            _factoryConnection = factoryConnection;
        }
        public async Task<int> Actualizar(Guid InstructorId, string nombre, string apellidos, string titulo)
        {
            var storeProcedure = "usp_instructor_editar";
            var resultado = 0;
            try{
                var connection =  _factoryConnection.GetConection();
                resultado = await connection.ExecuteAsync(storeProcedure, new{
                    InstructorId = InstructorId,
                    Nombre = nombre,
                    Apellidos = apellidos,
                    Titulo = titulo
                }, commandType : CommandType.StoredProcedure);

            }catch(Exception e){
                throw new Exception("No se pudo actualizar el instructor " + e);
            }finally{
                 _factoryConnection.CloseConnection();
            }
            return resultado;
        }

        public async Task<int> Eliminar(Guid id)
        {
            var storeProcedure = "usp_instructor_eliminar";
            var resultado = 0;
             try{
                var connection =  _factoryConnection.GetConection();
                resultado = await connection.ExecuteAsync(storeProcedure, new{
                    InstructorId = id}, commandType : CommandType.StoredProcedure);

            }catch(Exception e){
                throw new Exception("No se pudo eliminar el instructor " + e);
            }finally{
                 _factoryConnection.CloseConnection();
            }
            return resultado;
        }

        public async Task<int> Nuevo(string nombre, string apellidos, string titulo)
        {
            var storeProcedure = "usp_instructor_nuevo";
            var resultado = 0;
            try{
                var connection = _factoryConnection.GetConection();
                // ExecuteAsync va a devolder un valor de tipo entero indicando el nº de transacciones exitosas
                resultado = await connection.ExecuteAsync(storeProcedure, new{
                    InstructorId = Guid.NewGuid(),
                    Nombre = nombre,
                    Apellidos = apellidos,
                    Titulo = titulo
                },
                commandType : CommandType.StoredProcedure);

            }catch(Exception e){
                throw new Exception("No se ha podido crear el instructor" + e);
            }finally{
                 _factoryConnection.CloseConnection();
            }
            return resultado;
        }

        public async Task<IEnumerable<InstructorModelo>> ObtenerLista()
        {
            IEnumerable<InstructorModelo> instructorList = null;
            var storeProcedure = "usp_Obtener_Instructores";
            try{
                var connection = _factoryConnection.GetConection();
                // QueryAsync es porque trabajamos con transacciones asíncronas (es de Dapper)
                instructorList = await connection.QueryAsync<InstructorModelo>(storeProcedure,null,commandType : CommandType.StoredProcedure);
                // null es donde van los parámetros, el siguiente le indicamos que es un objeto store Procedure podría ser una query
            }catch(Exception e){
                throw new Exception("Error al mostrar la lista de Instructores", e);
            }finally{
                _factoryConnection.CloseConnection();
            }
            return instructorList;
        }

        public async Task<InstructorModelo> ObtenerPorId(Guid id)
        {
            
            var storeProcedure = "usp_Obtener_Instructor_Por_Id";
            InstructorModelo instructor = null;
            
            try{
                var connection = _factoryConnection.GetConection();
                // QueryAsync es porque trabajamos con transacciones asíncronas (es de Dapper)
                // InstructorModelo es para que mapee los datos que retorna el procedure
                instructor = await connection.QueryFirstAsync<InstructorModelo>(storeProcedure,
                new{
                    Id = id
                },commandType : CommandType.StoredProcedure);
                // null es donde van los parámetros, el siguiente le indicamos que es un objeto store Procedure podría ser una query
            }catch(Exception e){
                throw new Exception("Error al mostrar la lista de Instructores", e);
            }finally{
                _factoryConnection.CloseConnection();
            }
            return instructor;
        }
    }
}
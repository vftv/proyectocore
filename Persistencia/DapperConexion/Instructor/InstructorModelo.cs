using System;

namespace Persistencia.DapperConexion.Instructor
{
    public class InstructorModelo
    {
        // estos son los datos que va a devolver, los nombres deben coincidir con los nombres de las columnas en la tabla
        public Guid InstructorId {get;set;}

        public string Nombre {get;set;}

        public string Apellidos {get;set;}
        
        public string Titulo {get;set;}
    }
}
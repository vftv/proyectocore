using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Persistencia.DapperConexion.Instructor
{
    public interface IInstructor
    {
        // para obtener todos los instructores
        Task<IEnumerable<InstructorModelo>> ObtenerLista();

        // Para obtener un instructor por Id
        Task<InstructorModelo> ObtenerPorId(Guid Id);

        // Insertar uno nuevo, ponemos int porque la transaccion devuelve el numero de operaciones que se ejecutaron
        // correctamente
        Task<int> Nuevo(string nombre, string apellidos, string titulo);

        Task<int> Actualizar(Guid InstructorId, string nombre, string apellidos, string titulo);

        Task<int> Eliminar(Guid id);
    }
}
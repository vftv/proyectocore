using System.Data;

namespace Persistencia.DapperConexion
{
    public interface IFactoryConnection
    {
        // Este método es para cerrar las conexiones
        void CloseConnection();

        // creo que obtiene la conexión actual
        IDbConnection GetConection();
    }
}
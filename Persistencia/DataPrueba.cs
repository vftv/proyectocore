using System.Linq;
using System.Threading.Tasks;
using Dominio;
using Microsoft.AspNetCore.Identity;

namespace Persistencia
{
    public class DataPrueba
    {
        public static async Task InsertarData(CursosOnlineContext context, UserManager<Usuario> UsuarioManager){
            // Solo se tiene que ejecutar una vez, validamos si existe algún usuario en la BD
            if(!UsuarioManager.Users.Any()){
                var usuario = new Usuario{
                    NombreCompleto = "Antonio Lopez",
                    UserName = "Antolo",
                    Email = "Antonio.Lopez@gmail.com"
                    };
                    await UsuarioManager.CreateAsync(usuario,"Password123$");
            }
        }
    }
}
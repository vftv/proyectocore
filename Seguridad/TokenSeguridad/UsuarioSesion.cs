using System.Linq;
using System.Security.Claims;
using Aplicacion.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Seguridad.TokenSeguridad
{
    public class UsuarioSesion : IUsuariosSesion
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UsuarioSesion(IHttpContextAccessor httpContextAccesor){
            _httpContextAccessor = httpContextAccesor;
        }
        public string ObtenerUsuarioSesion()
        {
            // Los datos que almacenamos dentro de CoreIdentityUser se llama Claims
            var userName = _httpContextAccessor.HttpContext.User?.Claims?
                            .FirstOrDefault(x => x.Type==ClaimTypes.NameIdentifier)?.Value;
                            return userName;
        }
    }
}
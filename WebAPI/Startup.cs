using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplicacion.Contratos;
using Aplicacion.Cursos;
using Aplicacion.Interfaces;
using AutoMapper;
using Dominio;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Persistencia;
using Persistencia.DapperConexion;
using Persistencia.DapperConexion.Instructor;
using Seguridad.TokenSeguridad;
using WebAPI.Middleware;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
                public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Este método es el que agrega el funcionamiento los controlers en el proyectoa Entity Framework
             services.AddDbContext<CursosOnlineContext>(opt => {
                 //Configuration es quien tiene acceso a appsetting.json
                 opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
             });

             services.AddOptions();

            /* ConexionConfiguracion viene de Persistencia/DapperConexion, todo lo que se encuentre el la seccion
            ConnectionStrings se va a mapear en ConexionConfiguracion creando un objeto DefaultConnection que se
            encuentra dentro de ConnectionStrings*/
             services.Configure<ConexionConfiguracion>(Configuration.GetSection("ConnectionStrings"));
             
             // este servicio
             services.AddMediatR(typeof(Consulta.Manejador).Assembly);
            services.AddControllers(opt => { // aqui añadimos servicios a los controllers
                // esto es un servicio para pedir autenticacion en todas las llamadas
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                opt.Filters.Add(new AuthorizeFilter(policy));
            })
            .AddFluentValidation(cfg => cfg.RegisterValidatorsFromAssemblyContaining<Nuevo>());

            var builder = services.AddIdentityCore<Usuario>();
            var identityBuilder = new IdentityBuilder(builder.UserType, builder.Services);
            // A identityBuilder le agregamos la instancia de EntityFramework
            identityBuilder.AddEntityFrameworkStores<CursosOnlineContext>();
            // Aquí indicamos quién va a manejar el login
            identityBuilder.AddSignInManager<SignInManager<Usuario>>();
            services.TryAddSingleton<ISystemClock, SystemClock>();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Mi palabra secreta"));
            // Añadimos la seguridad de los Tokens
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt => {
                // Con esto decidimos quien o desde donde se pueden enviar tokens
                opt.TokenValidationParameters = new TokenValidationParameters{
                    // Cualquier tipo de request de un cliente tiene que ser validado del token y pasando el 
                    // IdentityCore
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = key,
                    ValidateAudience = false, // Quién va a poder crear estos Tokens(como de momento es para todos
                                              // pongo false, aunque podría poner IPs) y enviarnoslos
                    ValidateIssuer = false // Aquí validamos a que IPs enviamos el token, para todos = false
                };
            }
            );
            /* inyectamos la interface iJwtGenerador y la clase JwtGenerador que la implementa,
            los objetos Scoped son los mismos dentro de una solicitud, pero diferentes en diferentes solicitudes.*/
            services.AddScoped<iJwtGenerador, JwtGenerador>();

            // inyectamos la clase UsuarioSesion
            services.AddScoped<IUsuariosSesion, UsuarioSesion>();
            
            // añadimos AutoMapper y le decimos que procese los datos como 
            services.AddAutoMapper(typeof(Consulta.Manejador));

            /* Los objetos Transient(transitorios) siempre son diferentes 
             Se proporciona una nueva instancia a cada controlador y cada servicio.*/
            services.AddTransient<IFactoryConnection, FactoryConnection>();
            //services.AddScoped<IFactoryConnection, FactoryConnection>();
            services.AddScoped<IInstructor, InstructorRepositorio>();
        }

        /*This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        Este método determina como va a trabajar el proyecto dependiendo del entorno en el que estamos trabajando*/
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ManejadorErrorMiddleware>(); // Este es mi manejador de errores
            // Si el ambiente está en desarrollo...
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage(); // Este es el manejador de errores por defecto
            }

            //app.UseHttpsRedirection(); // Solo se usa en ambientes de producción, hay que comprar un certificado.

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

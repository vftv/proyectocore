using System;
using System.Net;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WebAPI.Middleware
{
    public class ManejadorErrorMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ManejadorErrorMiddleware> _logger;
        // Va a recibir 2 parámetros para poder manejar los estados de respuesta hacia el cliente
        public ManejadorErrorMiddleware(RequestDelegate next, ILogger<ManejadorErrorMiddleware> logger){
            _next = next;
            _logger = logger;
        }

        // Este método manejará request en el contexto
        public async Task Invoke(HttpContext context){
            try{
                // En context tenemos todos los datos que el usuario quiere introducir.
                await _next(context);
            }catch(Exception ex){
                await ManejadorExcepcionAsincrono(context, ex, _logger);
            }
        }

        private async Task ManejadorExcepcionAsincrono(HttpContext context,Exception ex,ILogger<ManejadorErrorMiddleware> _logger){
            object errores = null;
            switch(ex){
                case HandlerException me :
                    _logger.LogError(ex, "Manejador error");
                    errores = me._errores; // aquí guardo los errores
                    context.Response.StatusCode = (int)me._codigo;
                    break;
                case Exception e:
                    _logger.LogError(ex, "Error del servidor");
                    errores = string.IsNullOrWhiteSpace(e.Message) ? "Error" : e.Message;
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }
            context.Response.ContentType = "application/json";
            if(errores != null){
                // Para que esto funcione hay que instalat NewtonSoft.Json
                var resultados = JsonConvert.SerializeObject(new {errores});
                await context.Response.WriteAsync(resultados);
            }
        }
    }
}
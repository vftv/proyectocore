using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dominio;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Persistencia;

namespace WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Este método es el encargado de construir y ejecutar la aplicación
            var hostserver = CreateHostBuilder(args).Build();
            using(var ambiente = hostserver.Services.CreateScope()){
                var services = ambiente.ServiceProvider;
                try{
                    var userManager = services.GetRequiredService<UserManager<Usuario>>();
                    var context = services.GetRequiredService<CursosOnlineContext>();
                    context.Database.Migrate();
                    DataPrueba.InsertarData(context, userManager).Wait();
                }catch(Exception e){
                    var logging = services.GetRequiredService<ILogger<Program>>();
                    logging.LogError(e,"Ha ocurrido un error en la migración");
                }
            }
            hostserver.Run();
        }

        // Crea una configuración para que se ejecuten las webservices que posteriormente serán consumidas por un cliente
        // Si ejecutamos localmente la configuración la cargará de appsetting.Development.json
        // Si está publicado en Azure, se cargará appsetting.json
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    // Le pasamos a Stratup la configuración para que la inyecte
                    webBuilder.UseStartup<Startup>();
                });
    }
}
